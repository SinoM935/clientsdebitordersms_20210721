#select all clients
SELECT 
    *
FROM
    Warehouse.fact_daily_policy_snapshot
WHERE
    snapshot_date_key = '2021-07-20'
LIMIT 50;


SELECT 
    COUNT(*)
FROM
    Warehouse.fact_daily_policy_snapshot
WHERE
    snapshot_date_key = '2021-07-20'
    AND policy_status_key IN (2, 3, 4, 5);
 #should get 4,856 rows

#first get all devices and users in an active state
SELECT 
    a.device_key,
    b.device_barcode,
    c.first_name,
    c.last_name,
    c.mobile_number
FROM
    Warehouse.fact_daily_policy_snapshot a
        LEFT JOIN
    Warehouse.dim_devices b ON a.device_key = b.device_key
        LEFT JOIN
    Warehouse.dim_customers c ON a.customer_key = c.customer_key
WHERE
    a.snapshot_date_key = '2021-07-20'
        AND a.policy_status_key IN (2, 3, 4, 5)
;
#4,856 rows
#Save this data as is as a reference, then extract the distinct mobile numbers

SELECT DISTINCT
    c.mobile_number
FROM
    Warehouse.fact_daily_policy_snapshot a
        LEFT JOIN
    Warehouse.dim_devices b ON a.device_key = b.device_key
        LEFT JOIN
    Warehouse.dim_customers c ON a.customer_key = c.customer_key
WHERE
    a.snapshot_date_key = '2021-07-20'
        AND a.policy_status_key IN (2, 3, 4, 5) #removed and cancelled clients are excluded herre
;
#4,774 distinct mobiles with at least one active policy


#Exclusion 2: clients with applications from more than 5 months ago
#Do this exclusion by adding all clients in application for less than 5 months to my list

SELECT 
    a.device_key,
    b.device_barcode,
    c.first_name,
    c.last_name,
    c.mobile_number
FROM
    Warehouse.fact_daily_policy_snapshot a
        LEFT JOIN
    Warehouse.dim_devices b ON a.device_key = b.device_key
        LEFT JOIN
    Warehouse.dim_customers c ON a.customer_key = c.customer_key
WHERE
    a.snapshot_date_key = '2021-07-20'
        AND a.policy_status_key = 1 #policies in application
        AND a.system_created_date >= '2021-02-21' #application within the last 5 months
        ;
#1,829 "new" applications
#combine this applications list with the active list, and de-dupe by mobile number
 
 
#check how many distinct mobiles are in this list
SELECT DISTINCT
    c.mobile_number
FROM
    Warehouse.fact_daily_policy_snapshot a
        LEFT JOIN
    Warehouse.dim_devices b ON a.device_key = b.device_key
        LEFT JOIN
    Warehouse.dim_customers c ON a.customer_key = c.customer_key
WHERE
    a.snapshot_date_key = '2021-07-20'
        AND a.policy_status_key = 1
        AND a.system_created_date >= '2021-02-21'; 
        
#1,805 distinct mobiles
 
        
#exclusion 3: clients whose most recent payment was with debit order

#the below isn't the best way to go about most recent payments, but because of tabels not having indexes, use this for now

#Copy the list of device barcodes, these will be your debit order clients
#then check for their mobiles in the customer table, and exclude those mobiles from the list
(
SELECT 
    MAX(a.payment_date) AS most_recent_payment_date,
    a.device_key,
    b.device_barcode,
    c.payment_type,
    d.payment_status
FROM
    Warehouse.fact_collections a
        LEFT JOIN
    Warehouse.dim_devices b ON a.device_key = b.device_key
        LEFT JOIN
    Warehouse.dim_payment_types c ON a.payment_type_key = c.payment_type_key
        LEFT JOIN
    Warehouse.dim_payment_status d ON a.payment_status_key = d.status_key
WHERE
    is_current = 1
        AND d.payment_status = 'successful'
        AND c.payment_type = 'DEBIT_ORDER'
GROUP BY a.device_key , b.device_barcode);
#3310 